// SECTION: Common Imports
const path = require('path');

// SECTION: DB
const db = require('./db');

// SECTION: UI
const webServerSetup = async options => {
  const webpack = require('webpack');
  const webpackCompiler = webpack(require('./ui/webpack.config.js')(options));
  const webpackDevMiddleware = require('webpack-dev-middleware');

  if(!options.development) {
    webpackCompiler.run((err, stats) => {
      if (err) {
        console.error(err.stack || err);
        if (err.details) {
          console.error(err.details);
        }
        return;
      }

      const info = stats.toJson();

      if (stats.hasErrors()) {
        console.error(info.errors);
      }

      if (stats.hasWarnings()) {
        console.warn(info.warnings);
      }

      if(!stats.hasErrors()) {
        console.info('📦 UI Completed Compile');
      }
    });
  }


  // SECTION: Web Server
  const passport = require('passport');
  const express = require('express');
  const app = express();

  app.use(require('cookie-parser')());
  app.use(require('body-parser').urlencoded({extended: true}));
  app.use(require('body-parser').json());
  app.use(require('express-session')({
    name: 'pomby',
    secret: 'pomby sess',
    resave: false,
    saveUninitialized: true,
    cookie: {
      maxAge: options.sessionAge || 24 * 60 * 60 * 1000,
      httpOnly: true,
    },
  }));
  app.use(passport.initialize());
  app.use(passport.session());

  if(options.development) {
    app.use(webpackDevMiddleware(webpackCompiler, {publicPath: '/'}));
  }

  var expressStaticGzip = require('express-static-gzip');

  app.use(expressStaticGzip(path.resolve(__dirname, 'ui/build'), {enableBrotli: true}));

  app.use('/health.html',function(req, res, next) {
    res.json({status: 'healthy'});
  });

  app.use('/auth', require('./api/auth.js'));

  // Middleware
  app.use(require('./lib/middleware/auth.js'));

  // SECTION: GraphQL
  const {ApolloServer, gql} = require('apollo-server-express');
  const {typeDefs, resolvers} = require('./graph/index.js');

  const aServer = new ApolloServer({
    typeDefs: gql(typeDefs),
    resolvers,
    context: ctx => ({
      authUser: ctx.req.user,
      handlers: options.monitorHandlers,
    }),
    tracing: !!options.development,
    playground: {
      settings: {
        'request.credentials': 'same-origin',
      },
    },
  });

  aServer.applyMiddleware({app, path: '/query'});

  // non-graph APIs
  const reportsRouter = require('./api/reports/index.js');

  reportsRouter.setGraph(aServer);
  app.use('/reports', reportsRouter);

  return {app, aServer};
};

// SECTION: Service Worker
const service = require('./service/index.js');

module.exports = async options => {
  await db.init(options);
  const {app, aServer} = await webServerSetup(options);

  app.listen(options.port || 8080);
  console.info(`🔖 Web Server ready at http://localhost:${options.port || 8080}`);
  console.info(`🚀 Graph API ready at http://localhost:${options.port}${aServer.graphqlPath}`);
  service(options);
};
