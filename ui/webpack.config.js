const path = require('path');
const CopyPlugin = require('copy-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const CompressionPlugin = require('compression-webpack-plugin');
const VirtualModulesPlugin = require('webpack-virtual-modules');
const vModules = require('./webpack.vModules.js');
const zlib = require('zlib');

module.exports = options => {

  const virtualModules = new VirtualModulesPlugin(vModules(options));

  return {
    mode: options.development ? 'development' : 'production',
    entry: path.resolve(__dirname, 'src', 'index.jsx'),
    output: {
      path: path.resolve(__dirname, 'build'),
      filename: 'app.js',
      publicPath: '/',
    },
    performance: {
      hints: false, // turn on if we ever want to optimize
    },
    optimization: {
      minimize: !options.development,
      minimizer: [new TerserPlugin()],
    },
    devtool: 'source-map',
    module: {
      rules: [
        {
          test: /\.(jsx|js)$/,
          include: path.resolve(__dirname, 'src'),
          use: [{
            loader: 'babel-loader',
            options: {
              presets: [
                ['@babel/preset-env', {
                  'targets': 'defaults',
                }],
                '@babel/preset-react',
              ],
            },
          }],
        },
        {
          test: /\.svg$/,
          use: ['file-loader'],
        },
        {
          test: /\.css$/i,
          use: [MiniCssExtractPlugin.loader, 'css-loader'],
        },
        {
          test: /\.module\.s(a|c)ss$/,
          use: [
            MiniCssExtractPlugin.loader,
            {
              loader: 'css-loader',
              options: {
                modules: true,
                sourceMap: true,
              },
            },
            {
              loader: 'sass-loader',
              options: {
                sourceMap: true,
              },
            },
          ],
        },
        {
          test: /\.s(a|c)ss$/,
          exclude: /\.module.(s(a|c)ss)$/,
          use: [
            MiniCssExtractPlugin.loader,
            'css-loader',
            {
              loader: 'sass-loader',
              options: {
                sourceMap: true,
              },
            },
          ],
        },
      ],
    },
    plugins: [
      virtualModules,
      new CopyPlugin({
        patterns: [
        {from: path.resolve(__dirname, 'static'), to: path.resolve(__dirname, 'build')},
        ],
      }),
      new MiniCssExtractPlugin({
        filename: 'app.css',
        chunkFilename: '[id].css',
      }),
      new CompressionPlugin({
        filename: '[path][base].gz',
        test: /\.(js|css|html|svg|map)$/,
      }),
      new CompressionPlugin({
        filename: '[path][base].br',
        algorithm: 'brotliCompress',
        test: /\.(js|css|html|svg|map)$/,
        compressionOptions: {
          params: {
            [zlib.constants.BROTLI_PARAM_QUALITY]: 11,
          },
        },
      }),
    ],
  };
};
