const path = require('path');

module.exports = options => {
  const vModules = {};
  const loader = `
  let plugins = {};
  module.exports = plugins;
  `;

  vModules[`${path.join(__dirname, '../', 'node_modules/virtual-plugins.jsx')}`] = loader;

  return vModules;
};
