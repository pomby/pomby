import React, {useState, useContext} from 'react';
import {Link} from 'react-router-dom';
import UserContext from './components/userContext.jsx';
import useClickOutside from './lib/clickOutsideHook.jsx';
import {faTachometerAlt,faUser,faUsers,faHeartbeat,faSignOutAlt,faCodeBranch,faIdCard} from '@fortawesome/free-solid-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {gql, useQuery} from '@apollo/client';
import pkg from '../../package.json';

const USERCURRENT = gql`
  query GetUserCurrent {
    userCurrent {
      id
      login
      lastLogin
    }
  }
`;

function Nav(props) {
  const [active, setActive] = useState(false);
  const {ref: uDropRef, isVisible: uDropIsVisible, setIsVisible: uDropSetIsVisible} = useClickOutside(false);
  const userCtx = useContext(UserContext);

  const {error, data} = useQuery(USERCURRENT);

  if (error) {return <p>Error :(</p>;}

  return <nav className='navbar is-dark' role='navigation' aria-label='main navigation'>
    <div className='container'>
      <div className='navbar-brand'>
        <Link className='navbar-item' to='/'><h3 style={{padding: '0 20px'}}>POMBY</h3></Link>

        <a role='button' className={`navbar-burger burger ${active ? 'is-active' : ''}`}
          aria-label='menu'
          aria-expanded='false'
          onClick={() => {setActive(!active);}}>
          <span aria-hidden='true'></span>
          <span aria-hidden='true'></span>
          <span aria-hidden='true'></span>
        </a>
      </div>
      <div className={`navbar-menu ${active ? 'is-active' : ''}`}>
        <div className='navbar-start'>
          <Link className='navbar-item' to='/'><FontAwesomeIcon icon={faTachometerAlt} />&nbsp;Dashboard</Link>
          <Link className='navbar-item' to='/monitors'><FontAwesomeIcon icon={faHeartbeat} />&nbsp;Monitors</Link>
          {userCtx.hasPermission('useradmin') ? <Link className='navbar-item' to='/users'><FontAwesomeIcon icon={faUsers} />&nbsp;Users</Link> : ''}
        </div>
        <div className='navbar-end'>
        <div ref={uDropRef} className={`navbar-item has-dropdown ${uDropIsVisible ? 'is-active' : ''}`}>
            <a className='navbar-link' onClick={() => { uDropSetIsVisible(!uDropIsVisible); }}>
            <FontAwesomeIcon icon={faUser} />&nbsp;{data ? data.userCurrent.login : '...'}
            </a>

            <div className='navbar-dropdown is-right'>
              <Link className='navbar-item' to='/profile'><FontAwesomeIcon icon={faIdCard} />&nbsp;Profile</Link>
              <hr className='navbar-divider' />
              <a className='navbar-item' onClick={() => { userCtx.logout(); }}><FontAwesomeIcon icon={faSignOutAlt} />&nbsp;Logout</a>
              <hr className='navbar-divider' />
              <div className='navbar-item'>
               <FontAwesomeIcon icon={faCodeBranch} />&nbsp;
                Version {pkg.version}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </nav>;
};

export default Nav;
