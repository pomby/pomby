import React from 'react';
import {Link} from 'react-router-dom';
import toast from 'react-hot-toast';
import {gql, useQuery, useMutation} from '@apollo/client';
import {STATUS} from '../../../../lib/constants.js';
import Loader from '../../components/loader.jsx';

const MONITORLIST = gql`
  query GetMonitorList {
    monitorList {
      id
      title
      category
      currentStatus
    }
  }
`;

const DEL_MONITOR = gql`
  mutation MonitorDelete($id: String!) {
    monitorDelete(id: $id)
  }`;

function Monitors() {

  const {loading, error, data} = useQuery(MONITORLIST, {fetchPolicy: 'network-only'}); // No caching this is a light query
  const [monitorDelete] = useMutation(
    DEL_MONITOR,
    {
      onCompleted: () => { toast.success('Monitor Saved'); },
      onError: () => { toast.error('Error Saving Monitor'); },
    });

  if (loading) {return <Loader />;}
  if (error) {return <p>Error :(</p>;}

  const monitors = data.monitorList;

  return <section className='section'>
      <div className='level'>
        <div className='level-left'>
          <h2 className='title'>Monitors</h2>
        </div>
        <div className='level-right'>
          <Link to={'/monitor/new'} className='button is-primary'>New Monitor</Link>
        </div>
      </div>
      <table className='table is-striped is-hoverable is-fullwidth'>
        <thead>
          <tr>
            <th>Monitor</th>
            <th>Category</th>
            <th>Status</th>
            <th>&nbsp;</th>
          </tr>
        </thead>
        <tbody>
          {monitors.map((m, idx) => {
            let status = '';

            status += m.currentStatus & STATUS.success ? 'UP ' : '';
            status += m.currentStatus & STATUS.fail ? 'DOWN ' : '';
            status += m.currentStatus & STATUS.slow ? 'SLOW ' : '';
            status += m.currentStatus & STATUS.error ? 'ERROR ' : '';

            return <tr key={idx}>
              <td className='has-text-weight-semibold'><Link to={`/monitor/${m.id}/dashboard`}>{m.title}</Link></td>
              <td>{m.category}</td>
              <td>{status}</td>
              <td className='has-text-right'>
                <button className='delete is-medium'  onClick={() => {
                  monitorDelete({
                    variables: {id: m.id},
                    update: cache => {
                      const existing = cache.readQuery({query: MONITORLIST});

                      cache.writeQuery({
                        query: MONITORLIST,
                        data: {monitorList: existing.monitorList.filter(f => f.id !== m.id)},
                      });
                    },
                  });
                }} >Delete</button>
              </td>
            </tr>;
          })}
        </tbody>
      </table>
    </section>;

}

export default Monitors;
