import React, {useState} from 'react';
import {useParams} from 'react-router-dom';
import {gql, useQuery} from '@apollo/client';
import dayjs from 'dayjs';
import Tabs from './tabs.jsx';
import styles from './monitor.module.scss';
import Loader from '../../components/loader.jsx';
import QuickFormFieldHorizontal from './../../components/quickFormFieldHorizontal.jsx';

const MONITOR = gql`
  query GetMonitor($id: String!, $limit: Int!, $start: Date, $end: Date) {
    monitor (id:$id) {
      id
      title
      errorRecent(count: $limit, start: $start, end: $end){
        date
        details
      }
    }
  }
`;

function MonitorErrors() {

  let monitor = {};

  const [startD, setStartD] = useState(dayjs().subtract(7, 'day').format('YYYY-MM-DD HH:mm'));
  const [endD, setEndD] = useState(dayjs().format('YYYY-MM-DD HH:mm'));
  const [limit, setLimit] = useState(20);

  const {id} = useParams();
  const {loading, error, data} = useQuery(MONITOR, {variables: {id, limit: parseInt(limit), start: startD, end: endD}});

  if (loading) {return <Loader />;}
  if (error) {return <p>Error :(</p>;}

  monitor = data.monitor;

  const errorList = monitor.errorRecent || [];
  const reportVars = {
    id,
    start: startD,
    end: endD,
    count: parseInt(limit),
  };

  return <section className='section'>
      <Tabs active='errors' id={id} />

      <div className='level'>
        <div className='level-left'>
          <h2 className='title'>Monitor {monitor.title ? ' - ' + monitor.title : ''}</h2>
        </div>
      </div>
      <div className='columns'>
        <div className='column'>
          <QuickFormFieldHorizontal label='Start'>
            <input type='text' name='start' id='start' className='input'
              defaultValue={startD} onBlur={e => {setStartD(e.target.value);}} />
          </QuickFormFieldHorizontal>
        </div>
        <div className='column'>
          <QuickFormFieldHorizontal label='End'>
            <input type='text' name='end' id='end' className='input'
              defaultValue={endD} onBlur={e => {setEndD(e.target.value);}} />
          </QuickFormFieldHorizontal>
        </div>
        <div className={'column ' + styles['nospin']}>
          <QuickFormFieldHorizontal label='Count'>
            <input type='number' name='count' id='count' className='input'
              defaultValue={limit} onBlur={e => {setLimit(e.target.value);}}/>
          </QuickFormFieldHorizontal>
        </div>
      </div>
      <div className='columns'>
        <div className='column'>
          <h3 className='title is-4'>Recent Errors</h3>
          {errorList.map(el => <details key={el.date}>
            <summary>{el.date ? dayjs(el.date).format('YYYY-MM-DD HH:mm') : ''}</summary>
            <pre className={styles['prescroll']}><code>{el.details}</code></pre>
          </details>)}
          <h4></h4>
        </div>
      </div>
      <form method='post' action='/reports/run' target='_NEW'>
        <input type='hidden' name='report' value='errorSimple.docx' />
        <input type='hidden' name='vars' value={JSON.stringify(reportVars)} />
        <input type='submit' className={'button is-info'} value='Generate Report' />
      </form>
    </section>;

}

export default MonitorErrors;
