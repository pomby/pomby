import React, {useEffect} from 'react';
import * as d3 from 'd3';

function percentile(arr, p) {
  if (arr.length === 0) {return 0;}
  if (typeof p !== 'number') {throw new TypeError('p must be a number');}

  arr.sort((a,b) => a - b);

  if (p <= 0) {return arr[0];}
  if (p >= 1) {return arr[arr.length - 1];}

  var index = (arr.length - 1) * p,
    lower = Math.floor(index),
    upper = lower + 1,
    weight = index % 1;

  if (upper >= arr.length) {return arr[lower];}
  return arr[lower] * (1 - weight) + arr[upper] * weight;
}
const createGraph = (seriesRaw, startDate, classN) => {

  let seriesData = seriesRaw.map(s => {
    return {
      d: new Date(s.d),
      t: s.t,
      s: s.s,
      c: s.c,
      x: s.x,
      n: s.n,
    };
  });

  const margin = {top: 20, right: 20, bottom: 50, left: 70},
    width = 1280 - margin.left - margin.right,
    height = 500 - margin.top - margin.bottom;
  const x = d3.scaleTime().range([0, width]);
  const y = d3.scaleLinear().range([height, 0]);

  const valueLine = d3.line()
    .defined(d => d.s === d.c)
    .x(d => { return x(d.d); })
    .y(d => { return y(d.t); });

  const errorLine = d3.line()
    .defined((d, i, data) => {
      if(d.s !== d.c) {return true;}
      if(typeof data[i - 1] !== 'undefined' && data[i - 1].s !== data[i - 1].c) {return true;}
      if(typeof data[i + 1] !== 'undefined' && data[i + 1].s !== data[i + 1].c) {return true;}
      return false;
    })
    .x(d => { return x(d.d); })
    .y(d => { return y(d.t); });

  const tallness = d3.max(seriesData, d => { return d.t; });
  const band = d3.area()
    .x(d => x(d.d))
    .y0(d => y(d.x))
    .y1(d => y(d.n));

  const area = d3.area()
    .defined((d, i, data) => {
      if(d.s !== d.c) {return true;}
      if(typeof data[i - 1] !== 'undefined' && data[i - 1].s !== data[i - 1].c) {return true;}
      if(typeof data[i + 1] !== 'undefined' && data[i + 1].s !== data[i + 1].c) {return true;}
      return false;
    })
    .x(d => x(d.d))
    .y0(y(0))
    .y1(y(tallness));

  const svg = d3.select(classN).append('svg')
    .attr('viewBox', '0 0 ' + (width + margin.left + margin.right) + ' ' + (height + margin.top + margin.bottom))
    .attr('preserveAspectRatio', 'none')
    .append('g')
    .attr('transform', `translate(${margin.left}, ${margin.top})`);

  seriesData = seriesData.sort((a, b) => +a.d - +b.d);

  x.domain(d3.extent([...seriesData, {d: new Date(startDate)}], d => { return d.d; }));
  y.domain([0, percentile(seriesData.map(m=>m.x), 0.95)]);

  svg.append('path')
      .datum(seriesData)
      .attr('fill', 'rgba(255,0,0,0.15)')
      .attr('d', area);

  svg.append('path')
      .datum(seriesData)
      .attr('fill', 'rgba(55,55,55,0.5)')
      .attr('d', band);

  svg.append('path')
    .datum(seriesData)
    .attr('class', 'line')
    .attr('stroke', 'red')
    .attr('fill', 'none')
    .attr('stroke-width', 1)
    .attr('d', errorLine);

  svg.append('path')
    .datum(seriesData)
    .attr('class', 'line')
    .attr('stroke', 'white')
    .attr('fill', 'none')
    .attr('stroke-width', 1)
    .attr('d', valueLine);

  svg.append('g')
    .attr('transform', `translate(0, ${height})`)
    .call(d3.axisBottom(x));

  svg.append('g')
    .call(d3.axisLeft(y));
};

function GraphBand(props) {

  const classN = 'graphband' + Math.random().toString().substr(2, 8);

  useEffect(() => {
    createGraph(props.series, props.start, '.' + classN);
  }, []);

  return (
    <svg className={classN} height={500} width={'100%'}></svg>
  );
}

export default GraphBand;
