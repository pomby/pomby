import React from 'react';
import {Link} from 'react-router-dom';
import {faTachometerAlt,faBomb,faCogs} from '@fortawesome/free-solid-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';

function Tabs(props) {

  return <div className='tabs is-medium is-boxed'>
    <ul>
      {props.id !== 'new' ?
      <li className={props.active === 'dashboard' ? 'is-active' : ''}>
        <Link to={`/monitor/${props.id}/dashboard`}>
          <span className='icon is-small'><FontAwesomeIcon icon={faTachometerAlt} /></span>
          <span>Dashboard</span>
        </Link>
      </li> : ''}
      {props.id !== 'new' ?
      <li className={props.active === 'errors' ? 'is-active' : ''}>
        <Link to={`/monitor/${props.id}/errors`}>
          <span className='icon is-small'><FontAwesomeIcon icon={faBomb} /></span>
          <span>Errors</span>
        </Link>
      </li> : ''}
      <li className={props.active === 'config' ? 'is-active' : ''}>
        <Link to={`/monitor/${props.id}`}>
          <span className='icon is-small'><FontAwesomeIcon icon={faCogs} /></span>
          <span>Config</span>
        </Link>
      </li>
    </ul>
  </div>;
}

export default Tabs;
