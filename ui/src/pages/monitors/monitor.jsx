import React, {useState} from 'react';
import {useHistory, useParams} from 'react-router-dom';
import toast from 'react-hot-toast';
import Select from 'react-select';
import {gql, useQuery, useMutation} from '@apollo/client';
import Tabs from './tabs.jsx';
import QuickFormFieldHorizontal from './../../components/quickFormFieldHorizontal.jsx';
import Loader from '../../components/loader.jsx';
import {JsonEditor as Editor} from 'jsoneditor-react';
import 'jsoneditor-react/es/editor.min.css';
import ace from 'brace';
import 'brace/mode/json';
import 'brace/theme/github';
import styles from './monitor.module.scss';

const MONITOR = gql`
  query GetMonitor($id: String!) {
    monitor (id:$id) {
      id
      title
      type
      config
      notes
      category
      uri
    }
    handlerList{
      id
      title
    }
  }
`;

const MOD_MONITOR = gql`
  mutation MonitorUpdate($id: String, $title: String!, $type: String!, $config: String, $uri: String, $notes: String, $category: String) {
    monitorUpdate(monitor: {
      id: $id
      title: $title
      type: $type
      config: $config
      uri: $uri
      notes: $notes
      category: $category
    }){
      id
      title
      type
      config
      uri
      notes
      category
    }
  }
`;

let configValue = '';
const confValueUpdate = v => {
  configValue = JSON.stringify(v);
};

function Monitor() {

  let monitor = {};
  const {id} = useParams();
  const history = useHistory();
  const [typeOption, setTypeOption] = useState(null);

  const [monitorUpdate, {data: mutationData}] = useMutation(
    MOD_MONITOR,
    {
      onCompleted: () => { toast.success('Monitor Saved'); },
      onError: () => { toast.error('Error Saving Monitor'); },
    });

  if(mutationData) {
    monitor.id = mutationData.monitorUpdate.id;
    if(id === 'new') {
      history.push(`/monitor/${mutationData.monitorUpdate.id}`);
    }
  }

  const {loading, error, data} = useQuery(MONITOR, {variables: {id}, onCompleted: d => {
    if(d.monitor) {
      const handler = d.handlerList.find(f => f.id === d.monitor.type);

      setTypeOption({value: d.monitor.type, label: handler ? handler.title : d.monitor.type}); // Case in for if the handler is gone
    }else{
      setTypeOption({value: 'none', label: 'None'});
    }
  }});

  if (loading) {return <Loader />;}
  if (error) {return <p>Error :(</p>;}
  if (data && data.monitor) { monitor = data.monitor; }

  const typeOptions = data && data.handlerList ? data.handlerList.map(m => {return {value: m.id, label: m.title};}) : [];

  typeOptions.unshift({value: 'none', label: 'None'});

  let idField, titleField, categoryField, uriField, notesField;

  return <section className='section'>
      <Tabs active='config' id={id} />
      <form onSubmit={e => {
        e.preventDefault();
        monitorUpdate({variables: {
          id: idField.value,
          title: titleField.value,
          type: typeOption.value,
          category: categoryField.value,
          uri: uriField.value,
          notes: notesField.value,
          config: configValue,
        }});
      }}>
        <div className='level'>
          <div className='level-left'>
            <h2 className='title'>Monitor {monitor.title ? ' - ' + monitor.title : ''}</h2>
          </div>
          <div className='level-right'>
            <input type='submit' className={'button is-primary'} value='Save'/>
          </div>
        </div>

        <div className='columns'>
          <div className='column is-two-fifths'>
            <h3 className='title is-4'>Details</h3>
            <input type='hidden' name='monitorId' id='monitorId' value={monitor.id} ref={node => idField = node} />

            <QuickFormFieldHorizontal label='Title'>
              <input type='text' required={true} name='title' id='title' className='input'
                defaultValue={monitor.title}
                ref={node => titleField = node} />
            </QuickFormFieldHorizontal>

            <QuickFormFieldHorizontal label='Type'>
              <Select
                value={typeOption}
                onChange={setTypeOption}
                options={typeOptions}
                classNamePrefix='rSelect' />
            </QuickFormFieldHorizontal>

            <QuickFormFieldHorizontal label='Category'>
              <input type='text' name='category' id='category' className='input'
                defaultValue={monitor.category}
                ref={node => categoryField = node} />
            </QuickFormFieldHorizontal>

            <QuickFormFieldHorizontal label='URI'>
              <input type='text' name='uri' id='uri' className='input'
                defaultValue={monitor.uri}
                ref={node => uriField = node} />
            </QuickFormFieldHorizontal>

            <QuickFormFieldHorizontal label='Notes'>
              <textarea name='notes' id='notes' className='textarea'
                defaultValue={monitor.notes}
                ref={node => notesField = node} />
            </QuickFormFieldHorizontal>
          </div>

          <div className='column'>
            <h3 className='title is-4'>Config</h3>
            <Editor
              htmlElementProps={{style: {height: '500px'}}}
              value={monitor.config ? JSON.parse(monitor.config) : {}}
              onChange={confValueUpdate}
              ace={ace}
              mode='code'
              theme='ace/theme/github' />
          </div>
        </div>
    </form>
    </section>;

}

export default Monitor;
