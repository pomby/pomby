import React, {useEffect} from 'react';
import * as d3 from 'd3';

const createGraph = (seriesRaw, startDate) => {

  let seriesData = seriesRaw.map(s => {
    return {
      d: new Date(s.d),
      t: s.t,
      s: s.s,
    };
  });

  const margin = {top: 20, right: 20, bottom: 50, left: 70},
    width = 1280 - margin.left - margin.right,
    height = 500 - margin.top - margin.bottom;
  const x = d3.scaleTime().range([0, width]);
  const y = d3.scaleLinear().range([height, 0]);

  const valueLine = d3.line()
    .defined(d => !!(d.s & 1))
    .x(d => { return x(d.d); })
    .y(d => { return y(d.t); });

  const errorLine = d3.line()
    .defined((d, i, data) => {
      if(!(d.s & 1)) {return true;}
      if(typeof data[i - 1] !== 'undefined' && !(data[i - 1].s & 1)) {return true;}
      if(typeof data[i + 1] !== 'undefined' && !(data[i + 1].s & 1)) {return true;}
      return false;
    })
    .x(d => { return x(d.d); })
    .y(d => { return y(d.t); });

  const tallness = d3.max(seriesData, d => { return d.t; });
  const area = d3.area()
    .defined((d, i, data) => {
      if(!(d.s & 1)) {return true;}
      if(typeof data[i - 1] !== 'undefined' && !(data[i - 1].s & 1)) {return true;}
      if(typeof data[i + 1] !== 'undefined' && !(data[i + 1].s & 1)) {return true;}
      return false;
    })
    .x(d => x(d.d))
    .y0(y(0))
    .y1(y(tallness));

  const svg = d3.select('.graphit').append('svg')
    .attr('viewBox', '0 0 ' + (width + margin.left + margin.right) + ' ' + (height + margin.top + margin.bottom))
    .attr('preserveAspectRatio', 'none')
    .append('g')
    .attr('transform', `translate(${margin.left}, ${margin.top})`);

  seriesData = seriesData.sort((a, b) => +a.d - +b.d);

  x.domain(d3.extent([...seriesData, {d: new Date(startDate)}], d => { return d.d; }));
  y.domain([0, d3.max(seriesData, d => { return d.t; })]);

  svg.append('path')
      .datum(seriesData)
      .attr('fill', 'rgba(255,0,0,0.15)')
      .attr('d', area);

  svg.append('path')
    .datum(seriesData)
    .attr('class', 'line')
    .attr('stroke', 'red')
    .attr('fill', 'none')
    .attr('stroke-width', 1)
    .attr('d', errorLine);

  svg.append('path')
    .datum(seriesData)
    .attr('class', 'line')
    .attr('stroke', 'white')
    .attr('fill', 'none')
    .attr('stroke-width', 1)
    .attr('d', valueLine);

  svg.append('g')
    .attr('transform', `translate(0, ${height})`)
    .call(d3.axisBottom(x));

  svg.append('g')
    .call(d3.axisLeft(y));
};

function GraphWeek(props) {

  useEffect(() => {
    createGraph(props.series, props.start);
  }, []);

  return (
    <svg className='graphit' height={500} width={'100%'}></svg>
  );
}

export default GraphWeek;
