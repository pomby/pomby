import React, {useState} from 'react';
import {useParams} from 'react-router-dom';
import {gql, useQuery} from '@apollo/client';
import dayjs from 'dayjs';
import relativeTime from 'dayjs/plugin/relativeTime';
dayjs.extend(relativeTime);
import Tabs from './tabs.jsx';
import GraphWeek from './graphWeek.jsx';
import GraphBand from './graphBand.jsx';
import Loader from '../../components/loader.jsx';

const MONITOR = gql`
  query GetMonitor($id: String!, $start: Date!, $startHour: Date!, $startDay: Date!) {
    monitor (id:$id) {
      id
      title
      lastError
      created
      currentStatus
      timeseries(start:$start){
        t
        d
        s
      }
      timeseriesHour(start:$startHour){
        d
        s
        c
        t
        x
        n
      }
      timeseriesDay(start:$startDay){
        d
        s
        c
        t
        x
        n
      }
    }
  }
`;

function percentile(arr, p) {
  if (arr.length === 0) {return 0;}
  if (typeof p !== 'number') {throw new TypeError('p must be a number');}
  if (p <= 0) {return arr[0];}
  if (p >= 1) {return arr[arr.length - 1];}

  var index = (arr.length - 1) * p,
    lower = Math.floor(index),
    upper = lower + 1,
    weight = index % 1;

  if (upper >= arr.length) {return arr[lower];}
  return arr[lower] * (1 - weight) + arr[upper] * weight;
}

function median(values) {
  if(values.length === 0) {return 0;}

  values.sort((a,b) => a - b);

  var half = Math.floor(values.length / 2);

  if (values.length % 2) {return values[half];}

  return (values[half - 1] + values[half]) / 2.0;
}

function MonitorDashboard() {

  let monitor = {};
  const {id} = useParams();
  const [startMin, setStartMin] = useState(new Date().getTime() - 48 * 60 * 60 * 1000);
  const [startHour, setStartHour] = useState(new Date().getTime() - 7 * 24 * 60 * 60 * 1000);
  const [startDay, setStartDay] = useState(new Date().getTime() - 365 * 24 * 60 * 60 * 1000);

  if(id !== 'new') {
    const {loading, error, data} = useQuery(MONITOR, {variables: {
      id: id,
      start: startMin,
      startHour: startHour,
      startDay: startDay,
    }});

    if (loading) {return <Loader />;}
    if (error) {return <p>Error :(</p>;}

    monitor = data.monitor;
  }

  // Stats calcs
  const uptime48 = monitor.timeseries.reduce((ac, v) => ac + (v.s & 1), 0) / monitor.timeseries.length * 100;
  const uptime7 = monitor.timeseriesHour.reduce((ac, v) => ac + v.s, 0) / monitor.timeseriesHour.reduce((ac, v) => ac + v.c, 0)  * 100;
  const uptime1 = monitor.timeseriesDay.reduce((ac, v) => ac + v.s, 0) / monitor.timeseriesDay.reduce((ac, v) => ac + v.c, 0)  * 100;

  const rtSorted = monitor.timeseries.map(m=>m.t);

  rtSorted.sort((a,b) => a - b);
  const rtMedian = median(rtSorted); // mapped due to apollo objects being frozen
  const percent1 = percentile(rtSorted, 0.01);
  const percent5 = percentile(rtSorted, 0.05);
  const percent95 = percentile(rtSorted, 0.95);
  const percent99 = percentile(rtSorted, 0.99);

  let uptime = '--';

  if(monitor.currentStatus & 1) {
    if(monitor.lastError) {
      uptime = dayjs(monitor.lastError).toNow(true);
    }else{
      uptime = dayjs(monitor.created).toNow(true);
    }
  }

  return <section className='section'>
      <Tabs active='dashboard' id={id} />

      <div className='level'>
        <div className='level-left'>
          <h2 className='title'>Monitor {monitor.title ? ' - ' + monitor.title : ''}</h2>
        </div>
      </div>
      <div>
        <h3 className='title is-4'>Stats</h3>
        <nav className='level'>
        <div className='level-item has-text-centered'>
            <div>
              <p className='heading'>Uptime</p>
              <p className='title'>{uptime}</p>
            </div>
          </div>
          <div className='level-item has-text-centered'>
            <div>
              <p className='heading'>48h Availability</p>
              <p className='title'>{uptime48.toFixed(3)}%</p>
            </div>
          </div>
          <div className='level-item has-text-centered'>
            <div>
              <p className='heading'>7 Day Availability</p>
              <p className='title'>{uptime7.toFixed(3)}%</p>
            </div>
          </div>
          <div className='level-item has-text-centered'>
            <div>
              <p className='heading'>1 Year Availability</p>
              <p className='title'>{uptime1.toFixed(3)}%</p>
            </div>
          </div>
        </nav>
        <nav className='level'>
          <div className='level-item has-text-centered'>
            <div>
              <p className='heading'>Response Time (1%)</p>
              <p className='title'>{percent1.toFixed(1)}ms</p>
            </div>
          </div>
          <div className='level-item has-text-centered'>
            <div>
              <p className='heading'>Response Time (5%)</p>
              <p className='title'>{percent5.toFixed(1)}ms</p>
            </div>
          </div>
          <div className='level-item has-text-centered'>
            <div>
              <p className='heading'>Response Time (median)</p>
              <p className='title'>{rtMedian.toFixed(1)}ms</p>
            </div>
          </div>
          <div className='level-item has-text-centered'>
            <div>
              <p className='heading'>Response Time (95%)</p>
              <p className='title'>{percent95.toFixed(1)}ms</p>
            </div>
          </div>
          <div className='level-item has-text-centered'>
            <div>
              <p className='heading'>Response Time (99%)</p>
              <p className='title'>{percent99.toFixed(1)}ms</p>
            </div>
          </div>
        </nav>
      </div>
      <div>
        <h3 className='title is-4'>48 Hours</h3>
        <GraphWeek start={startMin} series={monitor.timeseries || []} />
      </div>
      <div>
        <h3 className='title is-4'>1 Week</h3>
        <GraphBand start={startHour} series={monitor.timeseriesHour || []} />
      </div>
      <div>
        <h3 className='title is-4'>1 Year</h3>
        <GraphBand start={startDay} series={monitor.timeseriesDay || []} />
      </div>
    </section>;

}

export default MonitorDashboard;
