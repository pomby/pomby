import React from 'react';
import {Link} from 'react-router-dom';
import toast from 'react-hot-toast';
import dayjs from 'dayjs';
import {gql, useQuery, useMutation} from '@apollo/client';
import Loader from '../../components/loader.jsx';

const USERLIST = gql`
  query GetUserList {
    userList {
      id
      login
      lastLogin
    }
  }
`;

const DEL_USER = gql`
  mutation UserDelete($id: String!) {
    userDelete(id: $id)
  }`;

function Users() {

  const {loading, error, data} = useQuery(USERLIST, {fetchPolicy: 'network-only'}); // No caching this is a light query
  const [userDelete] = useMutation(
    DEL_USER,
    {
      onCompleted: () => { toast.success('User Deleted'); },
      onError: () => { toast.error('Error Deleting User'); },
    });

  if (loading) {return <Loader />;}
  if (error) {return <p>Error :(</p>;}

  const users = data.userList;

  return <section className='section'>
      <div className='level'>
        <div className='level-left'>
          <h2 className='title'>Users</h2>
        </div>
        <div className='level-right'>
          <Link to={'/user/new'} className='button is-primary'>New User</Link>
        </div>
      </div>
      <table className='table is-striped is-hoverable is-fullwidth'>
        <thead>
          <tr>
            <th>Login</th>
            <th>Last Login</th>
            <th>&nbsp;</th>
          </tr>
        </thead>
        <tbody>
          {users.map((u, idx) => {
            let dt = '';

            if(u.lastLogin) {
              dt = new Date(u.lastLogin).toISOString();
            }
            return <tr key={u.id}>
              <td className='has-text-weight-semibold'><Link to={'/user/' + u.id}>{u.login}</Link></td>
              <td>{dt ? dayjs(dt).format('YYYY-MM-DD hh:mm a') : ''}</td>
              <td className='has-text-right'>
                <button className='delete is-medium' onClick={() => {
                  userDelete({
                    variables: {id: u.id},
                    update: cache => {
                      const existing = cache.readQuery({query: USERLIST});

                      cache.writeQuery({
                        query: USERLIST,
                        data: {userList: existing.userList.filter(f => f.id !== u.id)},
                      });
                    },
                  });
                }} >Delete</button>
              </td>
            </tr>;
          })}
        </tbody>
      </table>
    </section>;

}

export default Users;
