import React from 'react';
import {useParams} from 'react-router-dom';
import toast from 'react-hot-toast';
import {gql, useQuery, useMutation} from '@apollo/client';
import QuickFormFieldHorizontal from './../../components/quickFormFieldHorizontal.jsx';
import Loader from '../../components/loader.jsx';

const USER = gql`
  query GetUser($id: String!) {
    user (id:$id) {
      id
      login
      notes
      apikeyable
      permissions
    }
  }
`;

const MOD_USER = gql`
  mutation UserUpdate($id: String, $login: String, $password: String, $apikeyable: Boolean, $notes: String, $permissions: [String]) {
    userUpdate(user: {
      id: $id
      login: $login
      password: $password
      apikeyable: $apikeyable
      notes: $notes
      permissions: $permissions
    }){
      id
      login
      notes
      apikeyable
      permissions
    }
  }
`;

function User() {

  let user = {};
  const {id} = useParams();

  const [userUpdate, {data: mutationData}] = useMutation(
    MOD_USER,
    {
      onCompleted: () => { toast.success('User Saved'); },
      onError: () => { toast.error('Error Saving User'); },
    }
  );

  if(mutationData) {
    user.id = mutationData.userUpdate.id;
  }

  if(id !== 'new') {
    const {loading, error, data} = useQuery(USER, {variables: {id}});

    if (loading) {return <Loader />;}
    if (error) {return <p>Error :(</p>;}

    user = data.user;
  }

  let idField, loginField, passwordField, apikeyableField, notesField,
    userAdminPermission;

  return <section className='section'>
      <form onSubmit={e => {
        e.preventDefault();
        const permissions = [];

        if(userAdminPermission.checked) {permissions.push('useradmin');}

        userUpdate({
          variables: {
            id: idField.value,
            password: passwordField.value,
            login: loginField.value,
            notes: notesField.value,
            apikeyable: apikeyableField.checked || false,
            permissions: permissions,
          },
        });
      }}>
      <div className='level'>
        <div className='level-left'>
          <h2 className='title'>User</h2>
        </div>
        <div className='level-right'>
          <input type='submit' className={'button is-primary'} value='Save'/>
        </div>
      </div>
      <div className='columns'>
        <div className='column is-two-thirds-desktop'>
          <h3 className='title is-4'>Details</h3>
          <input type='hidden' name='userId' id='userId' value={user.id} ref={node => idField = node} />

          <QuickFormFieldHorizontal label='Username'>
            <input type='text' required={true} autoComplete={'off'}
              name='login'
              id='login'
              className='input'
              defaultValue={user.login}
              ref={node => loginField = node} />
          </QuickFormFieldHorizontal>
          <QuickFormFieldHorizontal label='Password'>
            <input type='password' autoComplete={'off'}
              name='changePassword'
              id='changePassword'
              className='input'
              ref={node => passwordField = node} />
          </QuickFormFieldHorizontal>
          <QuickFormFieldHorizontal label='Notes'>
            <textarea name='notes'
              id='notes'
              className='textarea'
              defaultValue={user.notes}
              ref={node => notesField = node} />
          </QuickFormFieldHorizontal>

        </div>
        <div className='column'>
          <h3 className='title is-4'>Access</h3>
          <div className='field'>
            <div className='control'>
              <label className='checkbox'>
                <input type='checkbox' name='apikeyable' id='apikeyable'
                    defaultChecked={user.apikeyable ? 'checked' : ''} className='form-check-input'
                    ref={node => apikeyableField = node} />
                &nbsp;Can use API
              </label>
            </div>
          </div>
          <div className='field'>
            <div className='control'>
              <label className='checkbox'>
                <input type='checkbox' name='apikeyable' id='apikeyable'
                    defaultChecked={user.permissions && user.permissions.includes('useradmin') ? 'checked' : ''} className='form-check-input'
                    ref={node => userAdminPermission = node} />
                &nbsp;User Admin
              </label>
            </div>
          </div>
        </div>
      </div>
    </form>
    </section>;

}

export default User;
