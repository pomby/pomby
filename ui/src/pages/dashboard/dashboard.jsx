import React, {useState} from 'react';
import {gql, useQuery} from '@apollo/client';
import Loader from '../../components/loader.jsx';
import styles from './dashboard.module.scss';
import Tile from './tile.jsx';

const MONITORLIST = gql`
  query GetMonitorList ($start: Date!) {
    monitorList {
      id
      title
      category
      currentStatus
      dayRecent {
        date
        min
        max
        avg
        uptime
      }
      hourRecent {
        date
        min
        max
        avg
        uptime
      }
      errorLastSimple {
        date
      }
      timeseries(start:$start){
        t
        d
        s
      }
    }
  }
`;

function Dashboard() {

  const [startMin, setStartMin] = useState(new Date().getTime() - 4 * 60 * 60 * 1000);
  const {loading, error, data} = useQuery(MONITORLIST, {fetchPolicy: 'network-only', variables: {start: startMin}}); // No caching this is a light query

  if (loading) {return <Loader />;}
  if (error) {return <p>Error :(</p>;}

  const monitors = data.monitorList;
  const categories = monitors.reduce((acc, v) => {
    const cat = acc.find(f => f.title === (v.category || 'Uncategorized'));

    if(!cat) {
      acc.push({title: v.category});
    }

    return acc;
  }, []);

  return <section className='section'>
      <h2 className='title'>Dashboard</h2>
      {categories.map(cat => {
        const catMons = monitors.filter(v => cat.title === (v.category || 'Uncategorized'));

        catMons.sort((a, b) => a.title.localeCompare(b.title, 'en', {sensitivity: 'base'}));

        return <div key={cat.title}>
          <h3 className='title is-4'>{cat.title}</h3>
          {catMons.map(mon => <Tile key={mon.id} {...mon} start={startMin} />)}
        </div>;
      })}
    </section>;

}

export default Dashboard;
