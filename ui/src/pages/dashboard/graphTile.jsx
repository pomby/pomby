import React, {useEffect} from 'react';
import * as d3 from 'd3';

const createGraph = (seriesRaw, startDate, selector) => {

  let seriesData = seriesRaw.map(s => {
    return {
      d: new Date(s.d),
      t: s.t,
      s: s.s,
    };
  });

  const width = 1000, height = 75;
  const x = d3.scaleTime().range([0, width]);
  const y = d3.scaleLinear().range([height, 0]);

  const valueLine = d3.line()
    .defined(d => !!(d.s & 1))
    .x(d => { return x(d.d); })
    .y(d => { return y(d.t); });

  const errorLine = d3.line()
    .defined((d, i, data) => {
      if(!(d.s & 1)) {return true;}
      if(typeof data[i - 1] !== 'undefined' && !(data[i - 1].s & 1)) {return true;}
      if(typeof data[i + 1] !== 'undefined' && !(data[i + 1].s & 1)) {return true;}
      return false;
    })
    .x(d => { return x(d.d); })
    .y(d => { return y(d.t); });

  const tallness = d3.max(seriesData, d => { return d.t; });
  const area = d3.area()
    .x(d => x(d.d))
    .y0(y(0))
    .y1(d => y(d.t));

  const svg = d3.select(selector).append('svg')
    .attr('viewBox', '0 0 ' + width + ' ' + height)
    .attr('preserveAspectRatio', 'none')
    .append('g');

  seriesData = seriesData.sort((a, b) => +a.d - +b.d);

  //note this added tiem may cause a bit of a gap at the beginning. TODO: do better
  x.domain(d3.extent([...seriesData, {d: new Date(startDate)}], d => { return d.d; }));
  y.domain([0, d3.max(seriesData, d => { return d.t; })]);

  /*svg.append('path')
      .datum(seriesData)
      .attr('fill', 'green')
      .attr('d', area);*/

  svg.append('path')
    .datum(seriesData)
    .attr('class', 'line')
    .attr('stroke', '#900')
    .attr('fill', 'none')
    .attr('stroke-width', 1)
    .attr('d', errorLine);

  svg.append('path')
    .datum(seriesData)
    .attr('class', 'line')
    .attr('stroke', '#666')
    .attr('fill', 'none')
    .attr('stroke-width', 1)
    .attr('d', valueLine);

  /*svg.append('g')
    .attr('transform', `translate(0, ${height})`)
    .call(d3.axisBottom(x));

  svg.append('g')
    .call(d3.axisLeft(y));*/
};

function GraphTile(props) {

  useEffect(() => {
    createGraph(props.series, props.start, '.graphit' + props.gkey);
  }, []);

  return (
    <svg className={'graphit' + props.gkey} height={75} width={'100%'}></svg>
  );
}

export default GraphTile;
