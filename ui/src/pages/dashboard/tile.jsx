import React from 'react';
import {useHistory} from 'react-router-dom';
import PropTypes from 'prop-types';
import {STATUS} from '../../../../lib/constants.js';
import GraphTile from './graphTile.jsx';
import styles from './dashboard.module.scss';

function Tile(props) {

  const history = useHistory();

  let status = '';

  status += props.currentStatus & STATUS.success ? 'UP ' : '';
  status += props.currentStatus & STATUS.fail ? 'DOWN ' : '';
  status += props.currentStatus & STATUS.testFail ? 'BAD RESPONSE ' : '';
  status += props.currentStatus & STATUS.slow ? 'SLOW ' : '';
  status += props.currentStatus & STATUS.error ? 'ERROR ' : '';

  let style = '';

  style = props.currentStatus & STATUS.slow ? styles['slow'] : style;
  style = props.currentStatus & STATUS.testFail ? styles['bad'] : style;
  style = props.currentStatus & STATUS.fail ? styles['down'] : style;

  return <div className={styles['tile'] + ' ' + style} onClick={() => {history.push(`/monitor/${props.id}/dashboard`);}}>
    <GraphTile gkey={props.id} series={props.timeseries} start={props.start} />
    <div className={'level ' + styles['header']}>
      <div className='level-left'>
        <h4 className={'title is-5 ' + styles['name']}>{props.title}</h4>
      </div>
      <div className='level-right'>
        <h4 className={'title is-3 ' + styles['status']}>{status}</h4>
      </div>
    </div>
    <footer className='columns'>
      <div className='column has-text-centered'>
        <div>
          <h6 className='heading'><time dateTime={new Date(props.hourRecent.date).toISOString()}>Hour</time></h6>
          <div>
            <span>{props.hourRecent.min.toFixed(1)}</span>
            <span className={styles['lgTxt']}>{props.hourRecent.avg.toFixed(1)}</span>
            <span>{props.hourRecent.max.toFixed(1)}</span>
          </div>
        </div>
      </div>
      <div className='column has-text-centered'>
        <div>
          <h6 className='heading'><time dateTime={new Date(props.dayRecent.date).toISOString()}>Day</time></h6>
          <div>
            <span>{props.dayRecent.min.toFixed(1)}</span>
            <span className={styles['lgTxt']}>{props.dayRecent.avg.toFixed(1)}</span>
            <span>{props.dayRecent.max.toFixed(1)}</span>
          </div>
        </div>
      </div>
      <div className='column has-text-centered'>
        <div>
          <h6 className='heading'>Last Error</h6>
          <div className={styles['lgTxt']}>{props.errorLastSimple ? new Date(props.errorLastSimple.date).toLocaleString() : ''}</div>
        </div>
      </div>
    </footer>
  </div>;

}

Tile.propTypes = {
  id: PropTypes.string,
  title: PropTypes.string,
  currentStatus: PropTypes.number,
  dayRecent: PropTypes.object,
  hourRecent: PropTypes.object,
  errorLastSimple: PropTypes.object,
};

export default Tile;
