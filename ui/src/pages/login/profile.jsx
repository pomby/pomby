import React from 'react';
import toast from 'react-hot-toast';
import {gql, useMutation} from '@apollo/client';
import QuickFormFieldHorizontal from './../../components/quickFormFieldHorizontal.jsx';

const MOD_PROFILE = gql`
  mutation UserProfile($password: String) {
    userProfileUpdate(user: {
      password: $password
    }){
      id
    }
  }
`;

function User() {

  const [userUpdate, {data: mutationData}] = useMutation(
    MOD_PROFILE,
    {
      onCompleted: () => { toast.success('User Saved'); },
      onError: () => { toast.error('Error Saving User'); },
    }
  );

  let passwordField;

  return <section className='section'>
      <form onSubmit={e => {
        e.preventDefault();

        userUpdate({
          variables: {
            password: passwordField.value,
          },
        });
      }}>
      <div className='level'>
        <div className='level-left'>
          <h2 className='title'>Profile</h2>
        </div>
        <div className='level-right'>
          <input type='submit' className={'button is-primary'} value='Save'/>
        </div>
      </div>
      <div className='columns'>
        <div className='column'>
          <h3 className='title is-4'>Profile Details</h3>

          <QuickFormFieldHorizontal label='Password'>
            <input type='password' autoComplete={'off'}
              name='changePassword'
              id='changePassword'
              className='input'
              ref={node => passwordField = node} />
          </QuickFormFieldHorizontal>

        </div>
      </div>
    </form>
    </section>;

}

export default User;
