import React from 'react';
import PropTypes from 'prop-types';
import {faUser, faKey} from '@fortawesome/free-solid-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {removeDirectivesFromDocument} from '@apollo/client/utilities';

class Login extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      error: false,
    };
  }

  tryLogin(e) {
    e.preventDefault();

    const formData = new URLSearchParams();

    formData.append('username', this.username.value);
    formData.append('password', this.password.value);

    fetch('/auth/login', {
      method: 'POST',
      credentials: 'same-origin',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: formData,
    }).then(response => {
      if(response.status === 401) {
        this.setState({
          error: 'Login failed!',
        });
        return false;
      }else{
        return response.json();
      }
    })
    .then(data => {
      if(data) {
        this.props.onSuccess(data);
      }
    });
  }

  componentDidMount() {
    document.getElementById('username').focus();
  }

  render() {
    return <div className='container'>
    <section className='section'>
    <div className='columns is-desktop'>
      <div className='column'></div>
      <div className='column is-half-desktop'>
        <h1 className='title'>Pomby Login</h1>
        <form onSubmit={e => this.tryLogin(e)} >
            {this.state.error ? <div className='notification is-danger' role='alert'>{this.state.error}</div> : ''}
            <div className='field'>
              <label htmlFor='username' className='label'>Username</label>
              <div className='control has-icons-left'>
                <span className='icon is-small is-left'><FontAwesomeIcon icon={faUser} /></span>
                <input type='text'
                  placeholder='Username'
                  required={true}
                  autoComplete={'off'}
                  name='username'
                  id='username'
                  className='input'
                  ref={form => this.username = form} />
                </div>
            </div>
            <div className='field'>
              <label htmlFor='password' className='label'>Password</label>
              <div className='control has-icons-left'>
                <span className='icon is-small is-left'><FontAwesomeIcon icon={faKey} /></span>
                <input type='password'
                  placeholder='Password'
                  required={true}
                  name='Password'
                  id='password'
                  className='input'
                  ref={form => this.password = form} />
              </div>
            </div>
            <div className='field is-grouped is-grouped-right'>
              <div className='control'>
                <input type='submit' className={'button is-primary'} value='Login'/>
              </div>
            </div>
        </form>
      </div>
      <div className='column'></div>
    </div>
    </section>
    </div>;
  }
}

Login.propTypes = {
  onSuccess: PropTypes.func,
};

export default Login;
