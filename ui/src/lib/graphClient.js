import {ApolloClient, InMemoryCache, ApolloLink, HttpLink} from '@apollo/client';
import {onError} from '@apollo/client/link/error';

let checkLoginFunc = null;

const link = new HttpLink({
  uri: '/query',
});

const errorLink = onError(({networkError}) => {
  if (networkError && networkError.statusCode === 401) {
    // User not logged in redirect to login page
    if(checkLoginFunc) {
      checkLoginFunc();
    }
  }
});

const client = new ApolloClient({
  uri: '/query',
  cache: new InMemoryCache(),
  link: ApolloLink.from([errorLink, link]),
});

client.checkLoginFunc = v => {
  checkLoginFunc = v;
};

export default client;
