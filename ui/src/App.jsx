import React from 'react';
import {
  HashRouter as Router,
  Switch,
  Route,
} from 'react-router-dom';
import {Toaster} from 'react-hot-toast';
import {ApolloProvider} from '@apollo/client';
import graphClient from './lib/graphClient.js';

import styles from './app.scss';

import UserContext, {PombyUser} from './components/userContext.jsx';
import PluginContext, {pluginSettings} from './components/pluginContext.jsx';
import Loader from './components/loader.jsx';
import Nav from './Nav.jsx';
import Login from './pages/login/login.jsx';
import Profile from './pages/login/profile.jsx';
import Dashboard from './pages/dashboard/dashboard.jsx';
import Users from './pages/users/users.jsx';
import User from './pages/users/user.jsx';
import Monitors from './pages/monitors/monitors.jsx';
import Monitor from './pages/monitors/monitor.jsx';
import MonitorDashboard from './pages/monitors/monitorDashboard.jsx';
import MonitorErrors from './pages/monitors/monitorErrors.jsx';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      user: undefined,
      loading: true,
    };
    this.userContext = new PombyUser();
  }

  componentDidMount() {
    this.userContext.setHook(u => { this.setLogin.call(this, u); });
    this.userContext.isLoggedIn(true);
    graphClient.checkLoginFunc(() => {
      if(this.state.user) { // extra check against an inf loop
        this.userContext.logout();
      }
    });
  }

  setLogin(u) {
    this.setState({
      user: u ? u : false,
      loading: false,
    });
  }

  render() {
    if(this.state.loading) {
      return <Loader />;
    }else if(!this.state.user) {
      return <Login onSuccess={() => {this.userContext.isLoggedIn(true);}}/>;
    }

    return <PluginContext.Provider value={pluginSettings}>
      <UserContext.Provider value={this.userContext}>
      <ApolloProvider client={graphClient}>
        <Toaster />
        <Router>
          <Nav />
          <div className='container'>
            <Switch>
              <Route path='/monitors'>
                <Monitors />
              </Route>
              <Route path='/monitor/:id/dashboard' component={MonitorDashboard} />
              <Route path='/monitor/:id/errors' component={MonitorErrors} />
              <Route path='/monitor/:id' component={Monitor} />
              <Route path='/profile'>
                <Profile />
              </Route>
              <Route path='/users'>
                <Users />
              </Route>
              <Route path='/user/:id'>
                <User />
              </Route>
              <Route path='/'>
                <Dashboard />
              </Route>
            </Switch>
          </div>
        </Router>
      </ApolloProvider>
      </UserContext.Provider>
      </PluginContext.Provider>;
  }
};

export default App;
