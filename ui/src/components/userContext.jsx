import React from 'react';
import {gql, useQuery} from '@apollo/client';
import client from '../lib/graphClient.js';

const UserContext = React.createContext(null);

const USERCURRENT = gql`
  query GetUserCurrent {
    userCurrent {
      id
      login
      lastLogin
      permissions
    }
  }
`;

export const PombyUser = class PUser {

  constructor() {
    this.currentUser = null;
    this.hook = null;
  }

  setHook(_hook) {
    this.hook = _hook;
  }

  login(username, password) {
    //TODO: Move login here
  }

  logout() {
    return fetch('/auth/logout', {
      method: 'POST',
      credentials: 'same-origin',
    }).then(() => {
      client.cache.reset(); // clear apollo now that the user no longer is logged in
      if(this.hook) { this.hook(null); }
    });
  }

  isLoggedIn(recheck) {
    return client.query({
      query: USERCURRENT,
    }).then(v => {
      if(v.data.userCurrent && v.data.userCurrent.login) {
        this.currentUser = v.data.userCurrent;
        if(this.hook) { this.hook(v.data.userCurrent); }
        return v.data.userCurrent;
      }
      if(this.hook) { this.hook(null); }
      return null;
    }).catch(e => {
      if(this.hook) { this.hook(null); }
      return null;
    });
  }

  hasPermission(perm) {
    if(!this.currentUser) {
      return false;
    }
    return this.currentUser.permissions.includes(perm);
  }

};

export default UserContext;
