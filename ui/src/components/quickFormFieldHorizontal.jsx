import React from 'react';

function QuickFormFieldHorizontal(props) {
  return <div className='field is-horizontal'>
    <div className='field-label is-normal'>
      <label className='label'>{props.label}</label>
    </div>
    <div className='field-body'>
      <div className='field'>
        <div className='control'>
          {props.children}
        </div>
      </div>
    </div>
  </div>;
}

export default QuickFormFieldHorizontal;
