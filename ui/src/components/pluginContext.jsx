import React from 'react';
import plugins from 'virtual-plugins.jsx';

const PluginContext = React.createContext(null);

export const pluginSettings = {
  plugins,
};

export default PluginContext;
