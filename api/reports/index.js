const express = require('express');
const router = express.Router();
const {createReport} = require('docx-templates');
const {runHttpQuery} = require('apollo-server-core');
const fs = require('fs');
const path = require('path');

let graphServer = null;

router.post('/', (req, res) => {
  res.json('reports');
});

router.post('/run', async(req, res) => {
  const template = fs.readFileSync(path.join(__dirname, '../../reports/errorSimple.docx'));

  const buffer = await createReport({
    template,
    cmdDelimiter: ['{#', '#}'],
    data: async query => {
      const contextValue = await graphServer.createGraphQLServerOptions(req, res);
      const finalResults = await runHttpQuery([req, res], {
        method: req.method,
        options: contextValue,
        query: {
          query: query,
          variables: JSON.parse(req.body.vars),
        },
        //request: convertNodeHttpToRequest(req)
      });

      return {
        vars: JSON.parse(req.body.vars),
        data: JSON.parse(finalResults.graphqlResponse).data,
      }; // TODO: Error handling for when we allow more then prebuilt reports
    },
  });

  //fs.writeFileSync('report.docx', buffer)
  res.writeHead(200, [
    ['Content-Type', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'],
    ['Content-Disposition', `attachment; filename=report.docx`],
  ]);
  res.end(Buffer.from(buffer, 'base64'));
});

router.setGraph = a => { graphServer = a; };

module.exports = router;
