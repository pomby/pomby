const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt');
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;

const db = require('../db');

passport.use(new LocalStrategy(
  {
    usernameField: 'username',
    passwordField: 'password',
  },
 async(username, password, done) => {

   const user = await db.User.findOne({login: username});

   if (!user) {
     return done(null, false, {message: 'Incorrect login.'});
   }

   const passCheck = await  bcrypt.compare(password, user.password);

   if(passCheck) {
     user.lastLogin = new Date();
     user.save();

     return done(null, {
       username: user.login,
       id: user._id,
       permissions: user.permissions.map(m => m),
     });
   }

   return done(null, false, {message: 'Incorrect login.'});
 }
));

passport.serializeUser(function(user, done) {
  done(null, user);
});

passport.deserializeUser(function(user, done) {
  done(null, user);
});

router.post('/login', passport.authenticate('local'), (req, res) => {
  res.json(req.user);
});

router.post('/logout', function(req, res) {
  req.logout();
  res.json(true);
});

router.get('/userinfo', function(req, res) {
  if(!req.user) {
    res.status(401);
    return res.json({});
  }
  res.json(req.user);
});

module.exports = router;
