const mongoose = require('mongoose');

const globalSchema = new mongoose.Schema({
  key: String,
  value: String,
});

module.exports = globalSchema;
