const mongoose = require('mongoose');

const monitorErrorSchema = new mongoose.Schema({
  monitor: {type: mongoose.Schema.Types.ObjectId, ref: 'Monitor'},
  runTime: Date,
  details: String,
});

module.exports = monitorErrorSchema;
