const mongoose = require('mongoose');

/*
 * note on unique: https://mongoosejs.com/docs/validation.html#the-unique-option-is-not-a-validator
 */

const userSchema = new mongoose.Schema({
  login: {type: String, required: true, unique: true, minLength: 1}, // Note: unique is not a validator, but will error on save
  password: String, // Always a bcrypt string
  permissions: [String],
  apikeyable: {type: Boolean, default: false}, // Is the user allowed to use the API by JWT
  secretKeys: [{
    secret: {type: String, required: true, minLength: 10},
    created: {type: Date, required: true},
    title: {type: String, required: true, minLength: 1},
    details: String,
  }], // Keys within the JWTs so we can revoke them
  notes: String,
  lastLogin: Date,
});

module.exports = userSchema;
