const mongoose = require('mongoose');

const monitorLogDaySchema = new mongoose.Schema({
  monitor: {type: mongoose.Schema.Types.ObjectId, ref: 'Monitor'},
  runTime: {type: Date, index: true},
  timingMin: Number,
  timingMax: Number,
  timingSuccess: Number,
  timingAll: Number,
  countSuccess: Number,
  count: Number, // used for weighting
});

module.exports = monitorLogDaySchema;
