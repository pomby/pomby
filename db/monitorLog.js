const mongoose = require('mongoose');

const monitorLogSchema = new mongoose.Schema({
  monitor: {type: mongoose.Schema.Types.ObjectId, ref: 'Monitor'},
  runTime: {type: Date, index: true},
  success: Boolean,
  status: Number,
  timing: Number,
});

module.exports = monitorLogSchema;
