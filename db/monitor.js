const mongoose = require('mongoose');

/*
 * note on unique: https://mongoosejs.com/docs/validation.html#the-unique-option-is-not-a-validator
 */

const monitorSchema = new mongoose.Schema({
  // Config based items
  title: {type: String, index: true, required: true, unique: true, minLength: 1}, // Note: unique is not a validator, but will error on save
  type: {type: String, index: true},
  category: String,
  uri: String,
  config: String,
  notes: String,

  // Testing and Setup
  tested: {type: Boolean, default: false},
  created: Date,
  modified: Date,
  deleted: Date,

  // Run Status
  lastRun: Date,
  lastError: Date,
  nextRun: Date,
  lastTiming: Number,
  currentStatus: Number,
});

module.exports = monitorSchema;
