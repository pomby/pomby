const mongoose = require('mongoose');
const initDB = require('./initDB.js');
const globalSchema = require('./global.js');
const userSchema = require('./user.js');
const monitorSchema = require('./monitor.js');
const monitorErrorSchema = require('./monitorError.js');
const monitorLogSchema = require('./monitorLog.js');
const monitorLogHourSchema = require('./monitorLogHour.js');
const monitorLogDaySchema = require('./monitorLogDay.js');

const db = {
  db: null,
  init: async options => {
    mongoose.connect(options.db, {useNewUrlParser: true, useUnifiedTopology: true});
    mongoose.set('useCreateIndex', true);
    db.db = mongoose.connection;

    db.db.on('error', console.error.bind(console, 'DB connection error:'));
    db.db.once('open', () => {
      console.info('💡 DB Connection Successful');
      initDB(db);
    });
  },
  Global: mongoose.model('Global', globalSchema),
  User: mongoose.model('User', userSchema),
  Monitor: mongoose.model('Monitor', monitorSchema),
  MonitorError: mongoose.model('MonitorError', monitorErrorSchema),
  MonitorLog: mongoose.model('MonitorLog', monitorLogSchema),
  MonitorLogHour: mongoose.model('MonitorLogHour', monitorLogHourSchema),
  MonitorLogDay: mongoose.model('MonitorLogDay', monitorLogDaySchema),
};

module.exports = db;
