const bcrypt = require('bcrypt');
const saltRounds = 10;

module.exports = async db => {

  const usrCount = await db.User.estimatedDocumentCount();

  if(usrCount <= 0) {
    const hash = await bcrypt.hash('admin', saltRounds);
    const initUser = new db.User({
      login: 'admin',
      password: hash,
      permissions: ['useradmin'],
    });

    await initUser.save();

    console.info('No users found adding basic admin user. Login using admin / admin and change this password');
  }

  const usrWithUserAdmin = await db.User.findOne({permissions: 'useradmin'});

  if(!usrWithUserAdmin) {
    const firstUser = await db.User.findOne();

    firstUser.permissions.push('useradmin');
    await firstUser.save();

    console.info('No user with useradmin permission, added it to: ' + firstUser.login);
  }


};
