
module.exports = {
  name: 'Example Validator',
  config: {
    type: 'example-validator-1',
  },
  validate: async() => { return true; },
  run: async config => {

    return  {
      timing: 12345, // How long the request took in ms
      success: false, // Was the response successful
      gotResponse: false, // was a response returned at all
      slow: false, // was the response slow
      error: true, // was there any error in the response
      validated: true, // were you able to validate the response
      errorReport: {}, // any report you would like to store for fiew later. Any serializable object can be sent
    };

  },
};
