const {ForbiddenError} = require('apollo-server-express');
const {User, UserList} = require('../../lib/model/user.js');

const userQueries = {

  userCurrent: (parent, args, context)=> {
    return User.init(context.authUser.id);
  },

  user: (parent, args)=> {
    return User.init(args.id);
  },

  userList: () => {
    return UserList.list();
  },

};

const userMutations = {

  userProfileUpdate: (parent, args, context) => {
    return User.updateProfile(context.authUser.id, args.user);
  },

  userUpdate: (parent, args, context) => {
    if(!context.authUser.permissions.includes('useradmin')) { return new ForbiddenError('Not Authorized');}

    return User.updateOrCreate(args.user);
  },

  userDelete: async(parent, args, context) => {
    if(!context.authUser.permissions.includes('useradmin')) { return new ForbiddenError('Not Authorized');}

    const usr = await User.init(args.id);

    return usr ? usr.delete() : false;
  },

};

module.exports = {userQueries, userMutations};
