const {GraphQLScalarType, Kind} = require('graphql');
const {importSchema} = require('graphql-import');

const dateScalar = new GraphQLScalarType({
  name: 'Date',
  description: 'Date custom scalar type',
  serialize(value) {
    return value.getTime(); // Convert outgoing Date to integer for JSON
  },
  parseValue(value) {
    return new Date(value); // Convert incoming integer to Date
  },
  parseLiteral(ast) {
    if (ast.kind === Kind.INT) {
      return new Date(parseInt(ast.value, 10)); // Convert hard-coded AST string to integer and then to Date
    }
    return null; // Invalid hard-coded value (not an integer)
  },
});

const {userQueries, userMutations} = require('./user/user.js');
const {monitorQueries, monitorMutations} = require('./monitor/monitor.js');
const {handlerQueries} = require('./handler/handler.js');

const typeDefs = importSchema(__dirname + '/schema.graphql');

const resolvers = {
  Query: {
    hello: () => {
      return 'Hello world!';
    },
    ...userQueries,
    ...monitorQueries,
    ...handlerQueries,
  },
  Mutation: {
    ...userMutations,
    ...monitorMutations,
  },
  Date: dateScalar,
};



module.exports = {typeDefs, resolvers};
