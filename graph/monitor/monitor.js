const {Monitor, MonitorList} = require('../../lib/model/monitor.js');

const monitorQueries = {

  monitor: (parent, args)=> {
    if(args.id === '' || args.id === 'new') {
      return null;
    }
    return Monitor.init(args.id);
  },

  monitorList: () => {
    return MonitorList.list();
  },

};

const monitorMutations = {

  monitorUpdate: (parent, args) => {
    return Monitor.updateOrCreate(args.monitor);
  },

  monitorDelete: async(parent, args) => {
    const mon = await Monitor.init(args.id);

    return mon.delete();
  },

};

module.exports = {monitorQueries, monitorMutations};
