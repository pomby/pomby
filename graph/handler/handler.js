const handlerQueries = {

  handlerList: (parent, args, context) => {
    return context.handlers.map(m => {
      return {
        id: m.config.type,
        title: m.name || m.config.type,
        type: 'monitor',
      };
    });
  },

};

module.exports = {handlerQueries};
