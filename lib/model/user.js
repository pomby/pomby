const bcrypt = require('bcrypt');
const db = require('../../db');
const saltRounds = 10;

const arrEquals = (a, b) =>
  a.length === b.length &&
  a.every((v, i) => v === b[i]);

class User {
  constructor(dbObj) {
    if(!dbObj) { // Used for creating a new user
      return;
    }

    this._dbObj = dbObj;

    this.id = dbObj._id;
    this.login = dbObj.login;
    this.apikeyable = dbObj.apikeyable;
    this.notes = dbObj.notes;
    this.lastLogin = dbObj.lastLogin;
    this.permissions = dbObj.permissions;
  }

  async validate() {
    // TODO: basics
    // TODO: unique login check
    return true;
  }

  async save() {
    if(!this._dbObj) {
      this._dbObj = await new db.User();
    }

    if (this._dbObj.login !== this.login) { this._dbObj.login = this.login.trim(); };
    if (this._dbObj.apikeyable !== this.apikeyable) { this._dbObj.apikeyable = this.apikeyable; };
    if (this._dbObj.notes !== this.notes) { this._dbObj.notes = this.notes; };
    if (!arrEquals(this._dbObj.permissions, this.permissions)) { this._dbObj.permissions = this.permissions; };

    await this._dbObj.save();

    if(!this.id) {
      this.id = this._dbObj._id;
    }

    return true;
  }

  async delete() {
    if(this._dbObj) {
      await this._dbObj.remove();
      return true;
    }
    return false;
  }

  async setPassword(newPass) {
    const hash = await bcrypt.hash(newPass, saltRounds);

    this._dbObj.password = hash;

    return this._dbObj.save();
  }

  static async init(id) {
    const userObj = await db.User.findById(id);

    if(!userObj) {
      return false;
    }

    return new User(userObj);
  }

  static async updateProfile(userId, usr) {
    // we only let people update some of their own permissions
    const userObj = await User.init(userId);

    if(userObj) {
      if(usr.password) {
        await userObj.setPassword(usr.password);
      }
    }

    return userObj;
  }

  static async updateOrCreate(usr) {
    let userObj = null;

    if(usr.id) { // Modify
      userObj = await User.init(usr.id);

      if(!userObj) {
        return false;
      }

      if (usr.login) { userObj.login = usr.login; };
      if (usr.apikeyable || usr.apikeyable === false) { userObj.apikeyable = usr.apikeyable; };
      if (usr.notes || usr.notes === '') { userObj.notes = usr.notes; };
      if (usr.permissions && Array.isArray(usr.permissions)) { userObj.permissions = usr.permissions; };

    }else { // Create
      userObj = new User(null);

      userObj.login = usr.login;
      userObj.apikeyable = usr.apikeyable || false;
      userObj.notes = usr.notes ? usr.notes : null;
      userObj.permissions = usr.permissions ? usr.permissions : [];
    }

    const valid = await userObj.validate();

    if(!valid) {
      return null;
    }

    await userObj.save();

    if(usr.password) {
      await userObj.setPassword(usr.password);
    }

    return userObj;
  }
}

class UserList {
  static async list() {
    const listObj = await db.User.find();
    const finalList = listObj.map(v => {
      return new User(v);
    });

    return finalList;
  }
}

module.exports = {User, UserList};
