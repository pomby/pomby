const db = require('../../db');

class Monitor {
  constructor(dbObj) {
    if(!dbObj) { // Used for creating a new monitor
      return;
    }

    this._dbObj = dbObj;

    this.id = dbObj._id;
    this.title = dbObj.title;
    this.type = dbObj.type;
    this.notes = dbObj.notes;
    this.config = dbObj.config;
    this.category = dbObj.category;
    this.uri = dbObj.uri;
    this.created = dbObj.created;
    this.modified = dbObj.modified;
    this.deleted = dbObj.deleted;
    this.lastRun = dbObj.lastRun;
    this.lastError = dbObj.lastError;
    this.nextRun = dbObj.nextRun;
    this.currentStatus = dbObj.currentStatus;
  }

  async timeseries(args) {
    const start = args.start ? new Date(args.start) : new Date(new Date().getTime() - 24 * 60 * 60 * 1000);
    const end = args.end ? new Date(args.end) : new Date();
    const ts = await db.MonitorLog
      .find({monitor: this.id, runTime: {$gte: start, $lte: end}}, {}, {sort: {'runTime': -1}})
      .limit(10080); // 7 day range limit

    if(ts) {
      return ts.map(m => {
        return {
          s: m.status,
          d: m.runTime,
          t: m.timing,
        };
      });
    }
    return [];
  }

  async timeseriesHour(args) {
    const start = args.start ? new Date(args.start) : new Date(new Date().getTime() - 24 * 60 * 60 * 1000);
    const end = args.end ? new Date(args.end) : new Date();
    const ts = await db.MonitorLogHour
      .find({monitor: this.id, runTime: {$gte: start, $lte: end}}, {}, {sort: {'runTime': -1}})
      .limit(10080); // 7 day range limit

    if(ts) {
      return ts.map(m => {
        return {
          d: m.runTime,
          s: m.countSuccess,
          c: m.count,
          t: m.timingSuccess,
          x: m.timingMax,
          n: m.timingMin,
        };
      });
    }
    return [];
  }

  async timeseriesDay(args) {
    const start = args.start ? new Date(args.start) : new Date(new Date().getTime() - 24 * 60 * 60 * 1000);
    const end = args.end ? new Date(args.end) : new Date();
    const ts = await db.MonitorLogDay
      .find({monitor: this.id, runTime: {$gte: start, $lte: end}}, {}, {sort: {'runTime': -1}})
      .limit(10080); // 7 day range limit

    if(ts) {
      return ts.map(m => {
        return {
          d: m.runTime,
          s: m.countSuccess,
          c: m.count,
          t: m.timingSuccess,
          x: m.timingMax,
          n: m.timingMin,
        };
      });
    }
    return [];
  }

  async errorRecent(args) {
    const start = args.start ? new Date(args.start) : new Date(new Date().getTime() - 7 * 24 * 60 * 60 * 1000);
    const end = args.end ? new Date(args.end) : new Date();
    const errList = await db.MonitorError.find({monitor: this.id, runTime: {$gte: start, $lte: end}}, {}, {sort: {'runTime': -1}}).limit(args.count || 20);

    if(errList) {
      return errList.map(m => {
        return {
          details: m.details,
          date: m.runTime,
        };
      });
    }
    return [];
  }

  async errorLastSimple() {
    const lastError = await db.MonitorLog.findOne({monitor: this.id, success: false}, {}, {sort: {'runTime': -1}});

    if(lastError) {
      return {
        date: lastError.runTime,
        status: lastError.status,
        timing: lastError.timing,
      };
    }

    return null;
  }

  async dayRecent() {
    const lastDay = await db.MonitorLogDay.findOne({monitor: this.id}, {}, {sort: {'runTime': -1}});

    if(lastDay) {
      return {
        date: lastDay.runTime,
        min: lastDay.timingMin,
        max: lastDay.timingMax,
        success: lastDay.timingSuccess,
        avg: lastDay.timingSuccess,
        uptime: lastDay.countSuccess / lastDay.count * 100,
      };
    }else{
      return {
        date: null,
        min: 0,
        max: 0,
        success: 0,
        avg: 0,
        uptime: 0,
      };
    }
  }

  async hourRecent() {
    const lastHour = await db.MonitorLogHour.findOne({monitor: this.id}, {}, {sort: {'runTime': -1}});

    if(lastHour) {
      return {
        date: lastHour.runTime,
        min: lastHour.timingMin,
        max: lastHour.timingMax,
        success: lastHour.timingSuccess,
        avg: lastHour.timingSuccess,
        uptime: lastHour.countSuccess / lastHour.count * 100,
      };
    }else{
      return {
        date: null,
        min: 0,
        max: 0,
        success: 0,
        avg: 0,
        uptime: 0,
      };
    }
  }

  async validate() {
    // TODO: basics
    return true;
  }

  async save() {
    const dt = new Date();

    if(!this._dbObj) {
      this._dbObj = await new db.Monitor();

      this._dbObj.created = dt;
      this._dbObj.nextRun = dt;
    }

    this._dbObj.modified = dt;

    if (this._dbObj.title !== this.title) { this._dbObj.title = this.title.trim(); };
    if (this._dbObj.type !== this.type) { this._dbObj.type = this.type; };
    if (this._dbObj.config !== this.config) { this._dbObj.config = this.config; };
    if (this._dbObj.notes !== this.notes) { this._dbObj.notes = this.notes; };
    if (this._dbObj.category !== this.category) { this._dbObj.category = this.category; };
    if (this._dbObj.uri !== this.uri) { this._dbObj.uri = this.uri; };

    await this._dbObj.save();

    if(!this.id) {
      this.id = this._dbObj._id;
    }

    return true;
  }

  async delete() {
    if(this._dbObj) {
      this._dbObj.deleted = new Date();
      await this._dbObj.save();
      return true;
    }
    return false;
  }

  static async init(id) {
    const monitorObj = await db.Monitor.findById(id);

    if(!monitorObj) {
      return false;
    }

    return new Monitor(monitorObj);
  }

  static async updateOrCreate(mon) {
    let monObj = null;

    if(mon.id) { // Modify
      monObj = await Monitor.init(mon.id);

      if(!monObj) {
        return false;
      }

      if (mon.title) { monObj.title = mon.title; };
      if (mon.type) { monObj.type = mon.type; };
      if (mon.config) { monObj.config = mon.config; };
      if (mon.notes) { monObj.notes = mon.notes; };
      if (mon.category) { monObj.category = mon.category; };
      if (mon.uri) { monObj.uri = mon.uri; };

    }else { // Create
      monObj = new Monitor(null);

      monObj.title = mon.title;
      monObj.type = mon.type;
      monObj.config = mon.config;
      monObj.notes = mon.notes;
      monObj.category = mon.category;
      monObj.uri = mon.uri;
    }

    const valid = await monObj.validate();

    if(!valid) {
      return null;
    }

    await monObj.save();

    return monObj;
  }
}

class MonitorList {
  static async list() {
    const listObj = await db.Monitor.find({deleted: null});
    const finalList = listObj.map(v => {
      return new Monitor(v);
    });

    return finalList;
  }
}

module.exports = {Monitor, MonitorList};
