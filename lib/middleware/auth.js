module.exports = (req, res, next) => {
  if(!req.user || !req.user.username) {
    res.status(401);
    return res.json({message: 'No user logged in'});
  }

  next();
};
