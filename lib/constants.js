const STATUS = Object.freeze({
  success: 1,
  fail: 2,
  noResponse: 4,
  error: 8,
  slow: 16,
  testFail: 32,
  badConfig: 64,
});

module.exports = {STATUS};
