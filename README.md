# Pomby

Pomby is a simple application monitoring system for your services. Track downtime including error and downtime reports. 

Pomby is designed to be a pluggable architecture to allow you to build any type of monitor you need, as well as add pluggable actions on your monitors.

## Getting Started

```js
const pomby = require('@pomby/pomby');
const pombySimple = require('@pomby/simplerequest');

pomby({
  db: 'mongodb://localhost/pomby',
  port: 8087,
  development: false,
  sessionAge: 24 * 60 * 60 * 1000,
  monitorHandlers: [pombySimple],
  dataRetention: 7 * 24 * 60 * 60 * 1000, // 1 week
  defaultRunSpacing: 1 * 60 * 1000, // 1 min
});

```

### db <string> - required
Database conenction string - mongo DB

### monitorHandlers <[handler]> - required
An array of monitor handlers that you wish to be available.

### port <number> - required
default: 8080

Required to set the port the application runs on.

### defaultRunSpacing <number> - required
default: 30 seconds (also the minimum setting)

This specifies how often the monitors will fun by default.

### development <boolean> - optional
Runs the project in development mode, live rebuilds of the UI and more debugging output.

### sessionAge <number> - optional
Login session age in ms
### dataRetentions <number> - optional
Time to retrain error logs and per-run spacing logs.  Hourly and daily records are stored indefinity. 

## What does it do?

### Dashboard
A quick view to how your monitored applications are doing right now.

![Dashboard](docs/img/dashboard.png)

### Monitors
Detailed statistics about your monitored application and reporting on that monitored service. 

![Monitor](docs/img/monitor-dashboard.png)


## Monitors

* [Simple Request](https://gitlab.com/pomby/simple-request) - Simple request library using node fetch

## Plugins

* [Console Alerts](https://gitlab.com/pomby/console-alerts) - Console alerts for your monitors
* [Elastic Alerts](https://gitlab.com/pomby/elastic-alerts) - Elastic / Kibana alerts for your monitors

### Plugin Hooks

The following are the types of hooks you can use when creating a plugin

* `onRunComplete(config, monitor, runLog, error, errorDetails)` - Hook runs after a run is completed for a monitor

## Reporting

Coming soon...
