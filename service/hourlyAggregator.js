const db = require('../db/index.js');

const hourlyAggregator = async() => {
  let lastStats = await db.Global.findOne({key: 'lastStatsRun'});

  if(!lastStats) {
    const mostRecentHour = new Date();

    mostRecentHour.setMinutes(0);
    mostRecentHour.setSeconds(0);
    mostRecentHour.setMilliseconds(0);

    lastStats = new db.Global();
    lastStats.key = 'lastStatsRun';
    lastStats.value = mostRecentHour.getTime();
    await lastStats.save();
  }

  const lastTime = new Date(parseInt(lastStats.value, 10));
  const nextTime = new Date(lastTime.getTime() + 60 * 60 * 1000);

  if(nextTime < new Date()) { // Has an hour passed since last run?

    console.info(`⏲️ Running hourly aggregate for: ${nextTime}`);

    const consolidated = await db.MonitorLog.aggregate([
      {'$match': {runTime: {'$gt': lastTime, '$lte': nextTime}}},
      {'$group': {
        '_id': '$monitor',
        'avg_timing': {'$avg': {'$cond': {if: {'$gt': ['$timing', 0]}, then: '$timing', else: null}}},
        'avg_timing_success': {'$avg': {'$cond': {if: '$success', then: '$timing', else: null}}},
        'min_timing': {'$min': {'$cond': {if: '$success', then: '$timing', else: null}}},
        'max_timing': {'$max': {'$cond': {if: '$success', then: '$timing', else: null}}},
        'count': {'$sum': 1},
        'count_success': {'$sum': {'$cond': {if: '$success', then: 1, else: 0}}},
      }},
    ]);

    for (const item of consolidated) {
      const hourlyStore = new db.MonitorLogHour();

      hourlyStore.monitor = item._id;
      hourlyStore.runTime = nextTime;
      hourlyStore.timingMin = item.min_timing || 0;
      hourlyStore.timingMax = item.max_timing || 0;
      hourlyStore.timingSuccess = item.avg_timing_success || -1;
      hourlyStore.timingAll = item.avg_timing;
      hourlyStore.countSuccess = item.count_success || 0;
      hourlyStore.count = item.count;

      hourlyStore.save();
    }

    lastStats.value = nextTime.getTime();
    await lastStats.save();
  }
};

module.exports = hourlyAggregator;
