const db = require('../db/index.js');

const cleaner = async options => {

  const dataRetention = (options ? options.dataRetention : false) || 48 * 60 * 60 * 1000;
  const hours = dataRetention / (60 * 60 * 1000);
  const oldest = new Date(new Date().getTime() - dataRetention);

  await db.MonitorLog.deleteMany({runTime: {$lte: oldest}}).then(results => {
    if(results.deletedCount > 0) {
      console.info(`🧹 Cleaned up records (${hours} hours): `, results.deletedCount);
    }
  }).catch(err => {
    console.warn('Failed to clean old monitor records');
  });

  await db.MonitorError.deleteMany({runTime: {$lte: oldest}}).then(results => {
    if(results.deletedCount > 0) {
      console.info(`🧹 Cleaned up errors 💥 (${hours} hours): `, results.deletedCount);
    }
  }).catch(err => {
    console.warn('Failed to clean old error records');
  });

};

module.exports = cleaner;
