const db = require('../db/index.js');

const hourlyAggregator = async() => {
  let lastStats = await db.Global.findOne({key: 'lastStatsRunDaily'});

  if(!lastStats) {
    const mostRecentDay = new Date();

    mostRecentDay.setHours(0);
    mostRecentDay.setMinutes(0);
    mostRecentDay.setSeconds(0);
    mostRecentDay.setMilliseconds(0);

    lastStats = new db.Global();
    lastStats.key = 'lastStatsRunDaily';
    lastStats.value = mostRecentDay.getTime();
    await lastStats.save();
  }

  const lastTime = new Date(parseInt(lastStats.value, 10));
  const nextTime = new Date(lastTime.getTime() + 24 * 60 * 60 * 1000);

  if(nextTime < new Date()) { // Has an hour passed since last run?

    console.info(`📅 Running daily aggregate for: ${nextTime}`);

    const consolidated = await db.MonitorLog.aggregate([
      {'$match': {runTime: {'$gt': lastTime, '$lte': nextTime}}},
      {'$group': {
        '_id': '$monitor',
        'avg_timing': {'$avg': {'$cond': {if: {'$gt': ['$timing', 0]}, then: '$timing', else: null}}},
        'avg_timing_success': {'$avg': {'$cond': {if: '$success', then: '$timing', else: null}}},
        'min_timing': {'$min': {'$cond': {if: '$success', then: '$timing', else: null}}},
        'max_timing': {'$max': {'$cond': {if: '$success', then: '$timing', else: null}}},
        'count': {'$sum': 1},
        'count_success': {'$sum': {'$cond': {if: '$success', then: 1, else: 0}}},
      }},
    ]);

    for (const item of consolidated) {
      const dailyStore = new db.MonitorLogDay();

      dailyStore.monitor = item._id;
      dailyStore.runTime = nextTime;
      dailyStore.timingMin = item.min_timing || 0;
      dailyStore.timingMax = item.max_timing || 0;
      dailyStore.timingSuccess = item.avg_timing_success || -1;
      dailyStore.timingAll = item.avg_timing;
      dailyStore.countSuccess = item.count_success || 0;
      dailyStore.count = item.count;

      dailyStore.save();
    }

    lastStats.value = nextTime.getTime();
    await lastStats.save();
  }
};

module.exports = hourlyAggregator;
