const db = require('../db/index.js');
const YAML = require('yaml');
const hourlyAggregator = require('./hourlyAggregator.js');
const dailyAggregator = require('./dailyAggregator.js');
const cleaner = require('./cleaner.js');
const {STATUS} = require('../lib/constants.js');

let RUNSPACING = 5 * 60 * 1000; //TODO: switch to an option

let handlers = [];

let plugins = [];

const runOnRunCompletes = async(config, monitor, runLog, error, errDetails) => {
  const orc = plugins.filter(f => f.onRunComplete);

  for(const orcRun of orc) {
    try{
      await orcRun.onRunComplete(config, monitor, runLog, error, errDetails);
    }catch(e) {
      console.warn(`💥 Error with plugin: ${orcRun.name}`, e);
    }
  }
};

const mainLoop = async() => {
  try{
    // first find never runs
    const nextRun = await db.Monitor.findOne({$or: [{lastRun: null}, {nextRun: {$lte: new Date()}}], deleted: null});

    if(nextRun) {
      console.info(`🏃 Running monitor: ${nextRun.title}`);
      // first we set the run time so if it fails it doesnt just keep happening
      const runTime = new Date();

      let nextTime = new Date(nextRun.nextRun.getTime() + RUNSPACING);
      // using DB value for semi-consistant spacing instead of just adding to when we ran
      // which will be a little over the desiered spacing on every run

      // below is for extended downtime like when the server is off
      if(nextTime < new Date()) {
        nextTime = new Date(runTime.getTime() + RUNSPACING);
      }

      nextRun.lastRun = runTime;
      nextRun.nextRun = nextTime;
      await nextRun.save();

      // run again shortly, so we can run in parallel but no risk of running something twice
      setTimeout(mainLoop, 1000);

      // Next validate and run the config
      try{
        const runHandler = handlers.find(f => f.config.type === nextRun.type);

        if(runHandler) {
          const runConfig = JSON.parse(nextRun.config);
          const results = await runHandler.run(runConfig, nextRun.uri); //TODO: pokemon this

          // Insert results into DB
          if(results) {
            let finalStatus = 0;

            if(results.success) {
              finalStatus = finalStatus | STATUS.success;
            }else if(results.validated === false) {
              finalStatus = finalStatus | STATUS.testFail;
              nextRun.lastError = runTime;
            }else{
              finalStatus = finalStatus | STATUS.fail;
              nextRun.lastError = runTime;
            }
            if(!results.gotResponse) {
              finalStatus = finalStatus | STATUS.noResponse;
            }
            if(results.slow) {
              finalStatus = finalStatus | STATUS.slow;
            }
            if(results.error) {
              finalStatus = finalStatus | STATUS.error;
            }

            nextRun.currentStatus = finalStatus;
            nextRun.lastTiming = results.timing;
            await nextRun.save();

            const runLog = new db.MonitorLog();

            runLog.monitor = nextRun._id;
            runLog.runTime = runTime;
            runLog.timing = results.timing;
            runLog.status = finalStatus;
            runLog.success = results.success;
            await runLog.save();

            if(!results.success) {
              // Theres an error log what we have to the report
              const errReport = new db.MonitorError();

              errReport.monitor = nextRun._id;
              errReport.runTime = runLog.runTime;
              if(results.errorReport) {
                errReport.details = YAML.stringify(results.errorReport);
              }else{
                errReport.details = 'No report provided.';
              }

              await errReport.save();

              //PLUGIN: onRunComplete
              runOnRunCompletes(runConfig, nextRun, runLog, errReport, results.errorReport);
            }else{
              //PLUGIN: onRunComplete
              runOnRunCompletes(runConfig, nextRun, runLog);
            }

          }else{
            nextRun.currentStatus = STATUS.error;
            await nextRun.save();

            const runLog = new db.MonitorLog();

            runLog.monitor = nextRun._id;
            runLog.runTime = runTime;
            runLog.timing = -1;
            runLog.success = false;
            runLog.status = STATUS.error;
            await runLog.save();
          }
        }else{
          console.warn(`Error finding runner for ${nextRun.title}`);
        }
      }catch(e) {
        console.warn(`Error reading config for ${nextRun.title}: `, e);
      }
    }else{
      // run again shortly because we failed
      setTimeout(mainLoop, 1000);
    }

  }catch(e) {
    // gotta catch them all
    console.error('Error in service worker running a monitor ', e);

    // run again shortly because we failed
    setTimeout(mainLoop, 1000);
  }

};

const statsLoop = async options => {

  await hourlyAggregator();
  await dailyAggregator();
  if(Math.random() > 0.95) { // 5% chance; we dont need to do it that often
    await cleaner(options);
  }

  setTimeout(() => { statsLoop(options); } , 60 * 1000);
};

module.exports = options => {
  if(options.defaultRunSpacing) {
    RUNSPACING = Math.max(options.defaultRunSpacing, 0.5 * 60 * 1000); // Max speed if 30s, no need to DOS anyone
  }

  handlers = [...handlers, ...options.monitorHandlers];
  plugins = options.plugins ? options.plugins : [];
  setTimeout(mainLoop, 10000);
  setTimeout(() => { statsLoop(options); }, 10000);
};
